#include "stdafx.h"
#include "container_atd.h"
#include "shape_atd.h"
#include <fstream>

using namespace std;

namespace simple_shapes
{
	void Out(shape &s, ofstream &out);
	void OutCircle(container &arr, ofstream &out) 
	{
		out << "Only circles." << endl;
		for (int i = 0; i < arr.len; i++) 
		{
			out << i << ": ";
			if (arr.cont[i]->k == shape::CIRCLE)
				Out(*(arr.cont[i]), out);
			else
				out << endl;
		}
	}
}