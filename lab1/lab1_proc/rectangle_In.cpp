#include "stdafx.h"
#include "rectangle_atd.h"
#include <fstream>

using namespace std;

namespace simple_shapes
{
	void In(rectangle **r, ifstream &in)
	{
		rectangle *temp = new rectangle;
		in >> temp->x1 >> temp->y1 >> temp->x2 >> temp->y2;
		*r = temp;
	}
}

