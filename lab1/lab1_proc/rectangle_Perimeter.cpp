#include "stdafx.h"
#include "rectangle_atd.h"
#include <fstream>

using namespace std;

namespace simple_shapes
{
	float Perimeter(rectangle *r)
	{
		float p = 2.0 * (abs(r->x1 - r->x2) + abs(r->y1 - r->y2));
		return p;
	}
}