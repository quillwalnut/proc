#include "stdafx.h"
#include "container_atd.h"
#include <fstream>

using namespace std;

namespace simple_shapes
{
	bool Compare(shape *first, shape *second);
	void Sort(container &arr)
	{
		for (int i = 0; i < arr.len - 1; i++) 
		{
			for (int j = i + 1; j < arr.len; j++) 
			{
				if (Compare(arr.cont[i], arr.cont[j])) 
				{
					shape *tmp = arr.cont[i];
					arr.cont[i] = arr.cont[j];
					arr.cont[j] = tmp;
				}
			}
		}
	}
}