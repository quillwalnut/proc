#include "stdafx.h"
#include "shape_atd.h"
#include <fstream>

using namespace std;

namespace simple_shapes
{
	void In(rectangle **r, ifstream &in);
	void In(circle **c, ifstream &in);
	void In(triangle **t, ifstream &in);

	shape* In(ifstream &in)
	{
		shape *sp;
		int k;
		int currColor;

		in >> k;

		switch (k)
		{
		case 1:
		{
			circle *c = NULL;
			In(&c, in);
			sp = new shape;
			sp->k = shape::key::CIRCLE;
			sp->obj = (void*)c;
			break;
		}
		case 2:
		{
			rectangle *r = NULL;
			In(&r, in);
			sp = new shape;
			sp->k = shape::key::RECTANGLE;
			sp->obj = (void*)r;
			break;
		}
		case 3:
		{
			triangle *t = NULL;
			In(&t, in);
			sp = new shape;
			sp->k = shape::key::TRIANGLE;
			sp->obj = (void*)t;
			break;
		}
		default:
			return 0;
		}		

		in >> currColor;
		in >> sp->density;

		switch (currColor)
		{
		case 1:
			sp->color = RED;
			break;
		case 2:
			sp->color = ORANGE;
			break;
		case 3:
			sp->color = YELLOW;
			break;
		case 4:
			sp->color = GREEN;
			break;
		case 5:
			sp->color = BLUE;
			break;
		case 6:
			sp->color = DARKBLUE;
			break;
		case 7:
			sp->color = VIOLET;
			break;
		}
		return sp;
	}
}
