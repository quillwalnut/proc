#include "stdafx.h"
#include "rectangle_atd.h"
#include <fstream>

using namespace std;

namespace simple_shapes
{
	void Out(rectangle *r, ofstream &out)
	{
		out << "It is Rectangle: x1 = " << r->x1 << ", y1 = " << r->y1 
			<< ", x2 = " << r->x2 << ", y2 = " << r->y2;
	}
}