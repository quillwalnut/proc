#include "stdafx.h"
#include "shape_atd.h"
#include <fstream>

using namespace std;

namespace simple_shapes
{
	void Out(rectangle *r, ofstream &out);
	void Out(circle *c, ofstream &out);
	void Out(triangle *t, ofstream &out);

	void Out(shape &s, ofstream &out)
	{
		switch (s.k)
		{
		case shape::key::CIRCLE:
		{
			circle *current�ircle = current�ircle = (circle*)(s.obj);
			Out(current�ircle, out);
			break;
		}
		case shape::key::RECTANGLE:
		{
			rectangle * currentRectangle = currentRectangle = (rectangle*)(s.obj);
			Out(currentRectangle, out);
			break;
		}
		case shape::key::TRIANGLE:
		{
			triangle * currentTriangle = currentTriangle = (triangle*)(s.obj);
			Out(currentTriangle, out);
			break;
		}
		default:
			out << "Incorrect figure!" << endl;
		}

		out << ", color = ";
		switch (s.color)
		{
		case RED:
			out << "RED";
			break;
		case ORANGE:
			out << "ORANGE";
			break;
		case YELLOW:
			out << "YELLOW";
			break;
		case GREEN:
			out << "GREEN";
			break;
		case BLUE:
			out << "BLUE";
			break;
		case DARKBLUE:
			out << "DARKBLUE";
			break;
		case VIOLET:
			out << "VIOLET";
			break;
		} 
		out << ", density = " << s.density << endl;
	}
}