#include "stdafx.h"
#include <iostream>
#include <fstream>
#include "container_atd.h"

using namespace std;

namespace simple_shapes
{
	void Init(container &arr);
	void Clear(container &arr);
	void In(container &arr, ifstream &in);
	void Out(container &arr, ofstream &out);
	void OutCircle(container &arr, ofstream &out);
}

using namespace simple_shapes;

int main(int argc, char* argv[])
{

	if (argc != 3)
	{
		cout << "Incorrect command line! "
			"Waited: command infile outfile" << endl;
		exit(1);
	}

	ifstream in(argv[1]);
	ofstream out(argv[2]);

	cout << "Start" << endl;
	container arr;
	Init(arr);

	In(arr, in);
	out << "Filled container." << endl;
	OutCircle(arr, out);

	Clear(arr);
	out << "Empty container." << endl;
	Out(arr, out);

	cout << "Stop" << endl;
    return 0;
}

