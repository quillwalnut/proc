#include "stdafx.h"
#include <fstream>
#include "circle_atd.h"

using namespace std;

namespace simple_shapes
{
	float Perimeter(circle *c)
	{
		float p = 3.14 * 2.0 * c->r;
		return p;
	}
}