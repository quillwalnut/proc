#include "stdafx.h"
#include "triangle_atd.h"
#include <fstream>

using namespace std;

namespace simple_shapes
{
	void Out(triangle *t, ofstream &out)
	{
		out << "It is Triangle: x1 = " << t->x1 << ", y1 = " << t->y1
			<< ", x2 = " << t->x2 << ", y2 = " << t->y2 << ", x3 = " << t->x3 << ", y3 = " << t->y3;
	}
}