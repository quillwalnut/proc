#include "stdafx.h"
#include <fstream>
#include "triangle_atd.h"

using namespace std;

namespace simple_shapes
{
	void In(triangle **t, ifstream &in)
	{
		triangle *temp = new triangle;
		in >> temp->x1 >> temp->y1 >> temp->x2 >> temp ->y2
			>> temp->x3 >> temp->y3;
		*t = temp;
	}
}

