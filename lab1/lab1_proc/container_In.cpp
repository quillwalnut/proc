#include "stdafx.h"
#include "container_atd.h"
#include <fstream>

using namespace std;

namespace simple_shapes
{
	shape *In(ifstream &in);

	void In(container &arr, ifstream &in)
	{
		while (!in.eof())
		{
			if (arr.len < arr.max_len)
			{
				if ((arr.cont[arr.len] = In(in)) != 0)
					arr.len++;
			}
			else
				break;
		}
	}
}