#include "stdafx.h"
#include <fstream>
#include "triangle_atd.h"

using namespace std;

namespace simple_shapes
{
	float Perimeter(triangle *t)
	{
		float a = sqrt(pow(t->x1 - t->x2, 2) + pow(t->y1 - t->y2, 2));
		float b = sqrt(pow(t->x2 - t->x3, 2) + pow(t->y2 - t->y3, 2));
		float c = sqrt(pow(t->x1 - t->x3, 2) + pow(t->y1 - t->y3, 2));
		return a + b + c;
	}
}