#include "stdafx.h"
#include "container_atd.h"
#include <fstream>

using namespace std;

namespace simple_shapes
{
	void Out(shape &s, ofstream &out);
	float Perimeter(shape &s);
	void Sort(container &arr);
	void Out(container &arr, ofstream &out)
	{
		out << "Container contains " << arr.len
			<< " elements." << endl;
		Sort(arr);
		for (int i = 0; i < arr.len; i++)
		{
			out << i << ": ";
			Out(*(arr.cont[i]), out);
			out << "perimeter = "
				<< Perimeter(*(arr.cont[i])) << endl;
		}
	}
}