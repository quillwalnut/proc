#include "stdafx.h"
#include "shape_atd.h"
#include "container_atd.h"

namespace simple_shapes
{
	void Init(container &arr)
	{
		arr.len = 0;
	}

	void Clear(container &arr)
	{
		for (int i = 0; i < arr.len; i++)
		{
			delete arr.cont[i];
		}
		arr.len = 0;
	}
}