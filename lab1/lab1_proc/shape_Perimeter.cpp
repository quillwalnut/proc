#include "stdafx.h"
#include "shape_atd.h"
#include <fstream>

using namespace std;

namespace simple_shapes
{
	float Perimeter(rectangle *r);
	float Perimeter(circle *c);
	float Perimeter(triangle *t);

	float Perimeter(shape &s)
	{
		switch (s.k)
		{
		case shape::key::CIRCLE:
		{
			circle *currentCircle = currentCircle = (circle*)(s.obj);
			return Perimeter(currentCircle);
		}
		case shape::key::RECTANGLE:
		{
			rectangle * currentRectangle = currentRectangle = (rectangle*)(s.obj);
			return Perimeter(currentRectangle);
		}
		case shape::key::TRIANGLE:
		{
		triangle * currentTriangle = currentTriangle = (triangle*)(s.obj);
			return Perimeter(currentTriangle);
		}
		default:
			return -1;
		}
	}
}