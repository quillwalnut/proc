#include "stdafx.h"
#include <fstream>
#include "circle_atd.h"

using namespace std;

namespace simple_shapes
{
	void Out(circle *c, ofstream &out)
	{
		out << "It is Circle: x = " << c->x << ", y = " 
			<< c->y << ", r = " << c->r;
	}
}