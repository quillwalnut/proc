#include "stdafx.h"
#include "shape_atd.h"
#include <fstream>

using namespace std;

namespace simple_shapes
{
	float Perimeter(shape &s);
	bool Compare(shape *first, shape *second) 
	{
		return Perimeter(*first) < Perimeter(*second);
	}
}