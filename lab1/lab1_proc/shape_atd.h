#ifndef __shape_atd__
#define __shape_atd__

#include "rectangle_atd.h"
#include "circle_atd.h"
#include "triangle_atd.h"

namespace simple_shapes
{
	// ���������, ���������� ��� ��������� ������
	enum colorEnum { RED, ORANGE, YELLOW, GREEN, BLUE, DARKBLUE, VIOLET };
	struct shape
	{
		// �������� ������ ��� ������ �� �����
		enum key { CIRCLE, RECTANGLE, TRIANGLE };
		key k; // ����
			   // ������������ ������������
		float density;
		void* obj;
		colorEnum color;
	};
} // end simple_shapes namespace
#endif