#include "stdafx.h"
#include <fstream>
#include "circle_atd.h"

using namespace std;

namespace simple_shapes
{
	void In(circle **c, ifstream &in)
	{
		circle *temp = new circle;
		in >> temp->x >> temp->y >> temp->r;
		*c = temp;
	}
}

